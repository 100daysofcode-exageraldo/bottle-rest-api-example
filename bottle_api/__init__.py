import bottle
from bottle_api.resources import define_routes
from dynaconf import settings


api = bottle.app()

@api.get('/status')
def server_status():
    return bottle.HTTPResponse(
        {'status': 'I\'m good, thanks'},
        200
    )

define_routes(api)
