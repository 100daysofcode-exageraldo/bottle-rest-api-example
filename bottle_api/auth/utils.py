import time
import functools

import jwt
import bottle
from dynaconf import settings


class AuthorizationError(Exception):
    pass


def build_profile(user):
    return {
        'id': str(user.id),
        'username': user.username,
        'exp': time.time()+settings.get('JWT.TOKEN_EXPIRE')
    }


def get_token_from_header():
    auth = bottle.request.headers.get(settings.get('JWT.HEADER_NAME'))
    if not auth:
        raise AuthorizationError(
           f'{settings.get("JWT.HEADER_NAME")} header is expected'
        )

    header_key, *token = auth.split()

    if header_key.lower() != settings.get('JWT.HEADER_TYPE'):
        raise AuthorizationError(
            f'{settings.get("JWT.HEADER_NAME")} problem'
        )

    elif len(token) != 1:
        raise AuthorizationError(
            'Token not found'
        )


    [token] = token
    return token


def get_jwt_credentials():
    token = get_token_from_header()
    credentials = jwt.decode(token, settings.get('JWT.SECRET'))
    return credentials


def jwt_required(func):
    @functools.wraps(func)
    def decorated(*args, **kwargs):
        try:
            token = get_token_from_header()
        except AuthorizationError as error:
            return bottle.HTTPResponse({'error': str(error)}, 400)

        try:
            jwt.decode(token, settings.get('JWT.SECRET'))
        except jwt.ExpiredSignature:
            return bottle.HTTPResponse({'error': 'token is expired'}, 401)
        except jwt.DecodeError as message:
            return bottle.HTTPResponse({'error': message}, 401)
        return func(*args, **kwargs)
    return decorated
