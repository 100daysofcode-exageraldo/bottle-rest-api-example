import hashlib

import bottle
import jwt

from bottle_api import serializers
from bottle_api import models
from bottle_api.auth import utils
from dynaconf import settings


bottle_auth = bottle.Bottle()


@bottle_auth.post('/login')
def log_in():
    data = bottle.request.json
    login_schema = serializers.LoginSchema()
    errors = login_schema.validate(data)
    if errors:
        return bottle.HTTPResponse({'error': errors}, 422)

    serial_data = login_schema.dump(data)
    current_user = models.User.find_by_credential(serial_data['username'])
    if not current_user:
        return bottle.HTTPResponse({'error': 'user not found'}, 404)

    password = hashlib.sha1(serial_data['password'].encode('utf-8')).hexdigest()
    if password != current_user.password:
        return bottle.HTTPResponse({'error': 'invalid username/password'}, 400)

    token = jwt.encode(
        utils.build_profile(current_user),
        settings.get('JWT.SECRET'),
        algorithm=settings.get('JWT.ALGORITHM')
    ).decode('utf-8')
    return bottle.HTTPResponse({'token': token}, 200)


@bottle_auth.post('/signin')
def sign_in():
    data = bottle.request.json
    signin_schema = serializers.SigninSchema()
    errors = signin_schema.validate(data)
    if errors:
        return bottle.HTTPResponse({'error': errors}, 422)

    serial_data = signin_schema.dump(data)
    serial_data['password'] = hashlib.sha1(
        data['password'].encode('utf-8')
    ).hexdigest()
    credentials_exists = models.User().credentials_exists(
        serial_data['username'],
        serial_data['email']
    )
    if credentials_exists:
        return bottle.HTTPResponse({'error': 'username/email already in use'}, 409)

    new_user = models.User(**serial_data).save()
    return bottle.HTTPResponse({'user': new_user.to_dict()}, 201)
