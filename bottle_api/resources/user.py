import bottle

from bottle_api import models
from bottle_api.auth import utils


users_resources = bottle.Bottle()

@users_resources.get('/users')
@utils.jwt_required
def get_all_users():
    return {'users': [u.to_dict() for u in models.User.objects]}


@users_resources.get('/users/<username>')
@utils.jwt_required
def get_user_by_id(username):
    user = models.User.objects(username=username)
    if user:
        return {'user': user.to_dict()}
    return bottle.HTTPResponse({}, 204)
