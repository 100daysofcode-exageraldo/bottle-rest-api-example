import bottle

from bottle_api import models
from bottle_api.auth import utils
from bottle_api import serializers


task_resources = bottle.Bottle()

@task_resources.get('/task')
@utils.jwt_required
def get_my_list():
    user_info = utils.get_jwt_credentials()
    # my_list = models.TodoList.find_by_id(user_info['id'])
    return {'users': [u.to_dict() for u in models.User.objects]}


@task_resources.post('/task')
@utils.jwt_required
def create_new_item():
    user_info = utils.get_jwt_credentials()
    data = bottle.request.json
    item_schema = serializers.NewTaskSchema()
    errors = item_schema.validate(data)
    if errors:
        return bottle.HTTPResponse({'error': errors}, 422)

    serial_data = item_schema.dump(data)
    new_item = models.Task(**serial_data).save()
    return bottle.HTTPResponse({'user': new_item.to_dict()}, 201)


@task_resources.post('/task/<item_id>')
@utils.jwt_required
def edit_item(item_id):
    data = bottle.request.json
    edit_schema = serializers.EditTaskSchema()
    errors = edit_schema.validate(data)
    if errors:
        return bottle.HTTPResponse({'error': errors}, 422)

    # TODO: SEARCH ITEM
    serial_data = edit_schema.dump(data)
    # TODO: EDIT ITEM
    return bottle.HTTPResponse({'user': new_item.to_dict()}, 201)


@task_resources.post('/task/<item_id>')
@utils.jwt_required
def delete_item(item_id):
    data = bottle.request.json
    del_schema = serializers.DeleteTaskSchema()
    errors = del_schema.validate(data)
    if errors:
        return bottle.HTTPResponse({'error': errors}, 422)

    # TODO: SEARCH ITEM
    serial_data = del_schema.dump(data)
    # TODO: DELETE ITEM
    return bottle.HTTPResponse({'user': new_item.to_dict()}, 201)
