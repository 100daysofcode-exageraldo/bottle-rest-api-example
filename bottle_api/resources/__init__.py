import bottle

from bottle_api import auth
from bottle_api.resources import user, task, board


def define_routes(api):
    api.merge(auth.bottle_auth)
    api.merge(user.users_resources)
    api.merge(task.task_resources)