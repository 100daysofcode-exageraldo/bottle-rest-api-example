from datetime import datetime

from mongoengine import ListField, ReferenceField, 
from mongoengine import Document, StringField, DateTimeField

from bottle_api.models.user import User


class Task(Document):
    title = StringField(required=True)
    description = StringField()
    owner = ReferenceField(User)
    created_at = DateTimeField(default=datetime.now)
    status = StringField(required=True)
    signed_to = ReferenceField(User)
    tags = ListField(StringField())
    priority = StringField(required=True)
    reported_by = ReferenceField(User)
    # comments = EmbeddedDocumentListField('TaskComment')

    def to_dict(self):
        return {
            'id': str(self.id),
            'title': self.title,
            'description': self.description,
            'status': self.status,
            'signed_to': str(self.signed_to.id),
            'tags': self.tags
        }

    @staticmethod
    def find_by_owner(id):
        current_user = Task.objects(id=id)
        return current_user

    def save(self, user, *args, **kwargs):
        self.owner = user
        if not self.reported_by:
            self.reported_by = user

        return super(Board, self).save(*args, **kwargs)