from datetime import datetime

from mongoengine import ReferenceField, BooleanField, ListField
from mongoengine import Document, StringField, DateTimeField

from bottle_api.models.user import User


BOARD_STATUS = [
    'active',
    'inactive',
    'archived'
]
TASK_STATUS_DEFAULT = [
    'BACKLOG',
    'ON HOLD',
    'IN PROGRESS',
    'DONE'
]
TASK_TYPE_DEFAULT = [
    'Task',
    'Bug'
]

class Board(Document):
    title = StringField(required=True)
    owner = ReferenceField(User)
    created_at = DateTimeField(default=datetime.now)
    status = StringField(
        required=True,
        choices=BOARD_STATUS
    )
    task_type = ListField(
        StringField(),
        default=TASK_TYPE_DEFAULT
    )
    is_private = BooleanField(required=True)
    team = ListField(ReferenceField(User))
    task_status = ListField(
        StringField(),
        default=TASK_STATUS_DEFAULT
    )

    def to_dict(self):
        team = [str(m.id) for m in self.team]
        return {
            'id': str(self.id),
            'title': self.title,
            'status': self.status,
            'owner': str(self.owner.id),
            'status': self.status,
            'private': self.private,
            'team': team,
            'task_status': self.task_status
        }

    def save(self, user, *args, **kwargs):
        self.owner = user
        self.team.append(user)

        return super(Board, self).save(*args, **kwargs)
