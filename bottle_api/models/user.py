from mongoengine import Document, StringField, EmailField, ObjectIdField
from mongoengine.queryset.visitor import Q


class User(Document):
    username = StringField(max_length=100, required=True, unique=True)
    password = StringField(max_length=100, required=True)
    email = EmailField(max_length=100, required=True, unique=True)

    def to_dict(self):
        return {
            'id': str(self.id),
            'username': self.username,
            'email': self.email
        }

    @staticmethod
    def credentials_exists(username, email):
        user = User.objects(
            Q(username=username) | Q(email=email))
        if user:
            return True
        return False

    @staticmethod
    def find_by_credential(identity):
        current_user = User.objects(
            Q(username=identity) | Q(email=identity)).first()
        return current_user