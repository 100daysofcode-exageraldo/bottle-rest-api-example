from marshmallow import Schema, fields, INCLUDE


class NewBoardchema(Schema):
    title = fields.String(required=True)
    description = fields.Email(required=True)

    class Meta:
        unknown = INCLUDE


class EditBoardchema(Schema):
    id = fields.String(required=True)
    title = fields.String()
    description = fields.String()

    class Meta:
        unknown = INCLUDE


class DeleteBoardchema(Schema):
    id = fields.String(required=True)

    class Meta:
        unknown = INCLUDE
