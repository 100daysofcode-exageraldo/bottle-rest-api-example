from marshmallow import Schema, fields, INCLUDE


class NewTaskSchema(Schema):
    title = fields.String(required=True)
    description = fields.Email(required=True)

    class Meta:
        unknown = INCLUDE


class EditTaskSchema(Schema):
    id = fields.String(required=True)
    title = fields.String()
    description = fields.String()

    class Meta:
        unknown = INCLUDE


class ChangeStatusSchema(Schema):
    id = fields.String(required=True)
    status = fields.String()

    class Meta:
        unknown = INCLUDE

class DeleteTaskSchema(Schema):
    id = fields.String(required=True)

    class Meta:
        unknown = INCLUDE
