from marshmallow import Schema, fields, INCLUDE


class UserSchema(Schema):
    username = fields.String(required=True)
    email = fields.Email(required=True)

    class Meta:
        unknown = INCLUDE


class LoginSchema(Schema):
    username = fields.String(required=True)
    password = fields.String(required=True)

    class Meta:
        unknown = INCLUDE


class SigninSchema(Schema):
    username = fields.String(required=True)
    password = fields.String(required=True)
    email = fields.Email(required=True)

    class Meta:
        unknown = INCLUDE