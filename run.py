from mongoengine import connect
from bottle_api import api
from dynaconf import settings


if __name__ == "__main__":
    connect(
        **settings.get('MONGODB')
    )

    api.run(
        **settings.get('SERVER')
    )
